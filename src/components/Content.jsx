import React from "react";
import { useSelector } from "react-redux";

export const Content = () => {
  const member = useSelector((state) => state.counterReducer.player);

  return (
    <div>
      {member.map((team) => {
        return <div key={team._id}>{team.sName}</div>;
      })}
    </div>
  );
};
