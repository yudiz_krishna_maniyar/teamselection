import React, { useEffect } from "react";
import { Table } from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";
import {
  increment,
  decrement,
  decrementcredit,
  player,
  Getallplayer,
  removeplayer,
  creaditremove,
} from "../Redux/Action";

const WK = () => {
  useSelector((state) => state.counterReducer.count);
  const credit = useSelector((state) => state.counterReducer.credit);

  const getallplayers = useSelector(
    (state) => state.counterReducer.getallplayer
  );
  const member = useSelector((state) => state.counterReducer.player);

  const wkplayers = getallplayers?.matchPlayer?.filter(
    (name) => name.eRole == "WK"
  );

  //console.log(credit);
  //console.log(wkplayers, "wk");
  //console.log(getallplayers);
  //console.log(member);

  const dispatch = useDispatch();

  // const [data, setData] = useState([]);

  const handleIncrement = (team) => {
    if (member.length !== 11  && credit > 8.5)  {
      dispatch(player(team));
      dispatch(increment(1));
      dispatch(decrementcredit(team.nFantasyCredit));
      team.disable = true;
    }
    else {
      alert("CAN NOT ADD MORE THAN 11 PLAYERS AND CREDIT MUST BE 0 OR MORE");
      team.disabled = false;
    }
  };

  const handledecrement = (team) => {
    //console.log(team);
    dispatch(creaditremove(team.nFantasyCredit));
     dispatch(decrement(1));
     dispatch(removeplayer(team));
    //console.log(removeplayer(team), "remove");
    team.disabled = false;
    team.disable = false;
  };

  useEffect(() => {
    dispatch(Getallplayer());
    // console.log(dispatch(Getallplayer()));
  }, []);

  return (
    <>
      <div>
        <h2>Select(1-3)WK</h2>
        <Table striped bordered hover size="sm">
          <thead>
            <tr>
              <th>Fliter</th>
              <th>selected by</th>
              <th>Point</th>
              <th>Credits</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            {wkplayers?.map((team) => {
              return (
                <tr key={team._id}>
                  <td>1</td>
                  <td>{team.sName}</td>
                  <td>{team.nScoredPoints}</td>
                  <td>{team.nFantasyCredit}</td>
                  <td>
                    {member.filter((item) => item._id == team._id).length ==
                    0 ? (
                      <button
                        onClick={() => handleIncrement(team)}
                        disabled={
                          member.filter((item) => item.eRole == "WK").length >=
                          3
                            ? true
                            : team.disable
                        }
                      >
                        ADD
                      </button>
                    ) : (
                      <button
                        onClick={() => handledecrement(team)}
                        disabled={team.disabled}
                      >
                        delete
                      </button>
                    )}
                  </td>
                </tr>
              );
            })}
          </tbody>
        </Table>
      </div>
    </>
  );
};

export default WK;
