import React, { useEffect, useState } from "react";
import Axios from "axios";
import { Table } from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";
import {
  increment,
  decrement,
  decrementcredit,
  player,
   Getallplayer,
  removeplayer,
  creaditremove,
} from "../Redux/Action";

export const Bats = () => {
  useSelector((state) => state.counterReducer.count);
  const member = useSelector((state) => state.counterReducer.player);
const credit = useSelector (state => state.counterReducer.credit);

const getallplayers = useSelector (state => state.counterReducer.getallplayer);

const wkplayers = getallplayers?.matchPlayer?.filter(
    (name) => name.eRole == "BATS"
  );
  //console.log(member);

  const dispatch = useDispatch();

  const [data, setData] = useState([]);

  const handleIncrement = (team) => {
    if (member.length !== 11 && credit > 8.5) {
      dispatch(player(team));
      dispatch(increment(1));
      dispatch(decrementcredit(team.nFantasyCredit));

      team.disable = true;
    } else {
      alert("CAN NOT ADD MORE THAN 11 PLAYERS AND CREDIT MUST BE 0 OR MORE");
      team.disabled = false;
    }
  };

  const handledecrement = (team) => {
    // console.log(team);

    dispatch(creaditremove(team.nFantasyCredit));

    dispatch(decrement(1));
    dispatch(removeplayer(team));

    team.disabled = false;
    team.disable = false;
  };

 useEffect (() => {
  dispatch (Getallplayer ());
  // console.log(dispatch(Getallplayer()));
}, []);

  return (
    <>
      <div>
        <h3>select(3-5)Bats</h3>
        <Table striped bordered hover size="sm">
          <thead>
            <tr>
              <th>Fliter</th>
              <th>selected by</th>
              <th>Point</th>
              <th>Credits</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            {wkplayers?.map((team) => {
              return (
                <tr key={team._id}>
                  <td>1</td>
                  <td>{team.sName}</td>
                  <td>{team.nScoredPoints}</td>
                  <td>{team.nFantasyCredit}</td>
                  <td>
                    {member.filter((item) => team._id == item._id).length ==
                    0 ? (
                      <button
                        onClick={() => handleIncrement(team)}
                        disabled={
                          member.filter((item) => item.eRole == "BATS")
                            .length >= 5
                            ? true
                            : team.disable
                        }
                      >
                        ADD
                      </button>
                    ) : (
                      <button
                        onClick={() => handledecrement(team)}
                        disabled={team.disabled}
                      >
                        delete
                      </button>
                    )}
                  </td>
                </tr>
              );
            })}
          </tbody>
        </Table>
      </div>
    </>
  );
};
