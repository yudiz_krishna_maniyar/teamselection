import React, { useState } from "react";
import { Nav, Navbar, Container, Button } from "react-bootstrap";
import { Link, useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";

export const Header = () => {
  const navigate = useNavigate();
  const member = useSelector((state) => state.counterReducer.player);
  const credit = useSelector (state => state.counterReducer.credit);

  const handleClick = () => {
    if (
      member.filter((item) => item.eRole == "WK").length >= 1 &&
      member.filter((item) => item.eRole == "ALLR").length >= 1 &&
      member.filter((item) => item.eRole == "BATS").length >= 3 &&
      member.filter((item) => item.eRole == "BWL").length >= 3 
     
    ) {
      navigate("data");
    } else {
      alert(
        "YOU HAVE MUST SELECT FROM ONE 'WK', ONE 'ALLR' , THREE FROM BATS AND BWL"
      );
    }
  };

  return (
    <>
      <Navbar bg="light" expand="lg">
        <Container fluid>
          <Navbar.Brand href="#">Navbar scroll</Navbar.Brand>
          <Navbar.Toggle aria-controls="navbarScroll" />
          <Navbar.Collapse id="navbarScroll">
            <Nav
              className="me-auto my-2 my-lg-0"
              style={{ maxHeight: "100px" }}
              navbarScroll
            >
              <Link className="link" to="/wk">
                WikectKeeper
              </Link>

              <Link className="link" to="/allr">
                {" "}
                AllRounder
              </Link>

              <Link className="link" to="/bats">
                Bats
              </Link>

              <Link className="link" to="/bwl">
                Bwl
              </Link>

              <button
                disabled={member.length === 11 ? false : true}
                onClick={() => handleClick()}
              >
                NEXT PAGE
              </button>
            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>
    </>
  );
};
