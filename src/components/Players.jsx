import React from "react";
import { useSelector } from "react-redux";

export const Players = () => {
  const counter = useSelector((state) => state.counterReducer.count);
  const credit = useSelector((state) => state.counterReducer.credit);
  const member = useSelector((state) => state.counterReducer.player);
  const teamname_one = member.filter(
    (item) => item.sTeamName == "Indian CC Vienna"
  );
  const teamname_two = member.filter((item) => item.sTeamName == "Pakistan CC");

  return (
    <div>
      <h1>players</h1>
      <h3>{counter}/11</h3>
      <h2>Creadit</h2>
      <h3>{credit}</h3>
      <h4>india {teamname_one.length}</h4>
      <h4>pakistan {teamname_two.length}</h4>
    </div>
  );
};
