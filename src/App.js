
import './App.css';
import WicketKeeper from './components/WicketKeeper';
import { Header } from './components/Header';
import {Route, Routes} from "react-router-dom"
import AllRounder from './components/AllRounder.jsx';
import { Bats } from './components/Bats';
import Bwl from './components/Bwl';
import { Players } from './components/Players';
import { Content } from './components/Content';



function App() {
  return (
    <div className="App">
  <Players/>
    <Header />

    <Routes>
      <Route  path="/wk" 
     
      element={<WicketKeeper />}/>

      <Route path="/allr" element={<AllRounder />} />

      <Route path="/bats" element={<Bats />} />

      <Route path="/bwl" element={<Bwl />} />
    
        <Route path="/data" element={<Content />} />


      


    </Routes>
    
     
    </div>

  );
}

export default App;
