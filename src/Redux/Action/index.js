import axios from 'axios';
import {INCREMENT,DECREMENT,ADDCREADIT,REMOVECREADIT, GETALLPLAYER, GETALLPLAYERFAIL,ADDPLAYER, REMOVEPLAYER} from './ActionType';

export const increment = number => {
  return {
    type: INCREMENT,
    payload: number,
  };
};
export const decrement = number => {
  return {
    type: DECREMENT,
    payload: number,
  };
};
export const decrementcredit = number => {
  return {
    type: ADDCREADIT,
    payload: number,
  };
};

export const creaditremove = number => {
  return {
    type: REMOVECREADIT,
    payload: number,
  };
};


export const Getallplayer = ()=> async (dispatch)=>{
  try {
    
    const res = await axios.get("http://localhost:4000/data");
    dispatch({
    type: GETALLPLAYER,
    payload: res.data,
    })
  } catch (error) {
    dispatch({
      type: GETALLPLAYERFAIL,
      payload: error.response ? error.response.statusText : error.message,
    })
  }
  
};

export const player = play => {
  return {
    type: ADDPLAYER,
    payload: play,
  };
};

export const removeplayer = play => {
  
  return {
    type: REMOVEPLAYER,
    payload: play,
  };
};



