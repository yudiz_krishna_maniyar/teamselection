  import axios from "axios";
import { INCREMENT, GETALLPLAYER,GETALLPLAYERFAIL,DECREMENT,ADDCREADIT,ADDPLAYER,REMOVECREADIT,REMOVEPLAYER } from "../Action/ActionType";

const initialState = {
  count: 0,
  credit :100,
  teamname:0,
  player:[],
  getallplayer:[],
  loading: false,
  error: null,

};

export const counterReducer = (state = initialState, action) => {  

  switch (action.type) {
    case INCREMENT:
      return {
        ...state,
        count: state.count + action.payload
      };
      case GETALLPLAYER :
        return{
          ...state,
          getallplayer: action.payload,
          loading:false,
         
        };
         case GETALLPLAYERFAIL :
        return{
          ...state,
          error: action.payload,
          loading:false,
         
        };
      case DECREMENT:
      return {
        ...state,
        count: state.count - action.payload
      };
        
  case ADDCREADIT:
      return {
        ...state,
        credit: state.credit - action.payload
      };
      case REMOVECREADIT:
      return {
        ...state,
        credit: state.credit + action.payload
      };
     
      case ADDPLAYER:
        //console.log(action.payload);
      return {
        ...state,
        player: [...state.player , action.payload]
         
      };
      case REMOVEPLAYER:
     // console.log(action.payload);
    return {
        ...state,
      //  player:[...state.player - action.payload]
        player:[...state.player.filter((player)=>player._id !==action.payload._id)]

      };
        default:
      return state;
  }
 

};
